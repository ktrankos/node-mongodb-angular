var express = require('express');
var demo = require('./ats/demo');
var app = express();
var template  = require('swig');
var grab = require('./ats/grab');
var nunjucks  = require('nunjucks');
var customTpl = require('./ats/tpl/CustomTag'); 
app.use(express.static(__dirname + '/public'));
//app.set('view engine', 'jade');
app.set('views', './views');
var env = nunjucks.configure('views', {
  autoescape: true,
  express   : app,
  tags: {
      blockStart: '<%',
      blockEnd: '%>',
      variableStart: '<$',
      variableEnd: '$>',
      commentStart: '<#',
      commentEnd: '#>'
    }
  
});
//var env = nunjucks.configure('views');
env.addFilter('shorten', function(str, count) {
    return str.slice(0, count || 5);
});

env.addFilter('formfield', customTpl.form);
/*
env.addFilter('formfield', function(field, vars) {
    console.log(field);
    switch(field.type)
    {
      case 'text':

      t =  '<input type="text" name="'+field.name+'" />';
      break;
    }
    return new nunjucks.runtime.SafeString(t);
});
*/
app.engine('nunj', nunjucks.render);
app.set('view engine', 'nunj');
app.use(function (req, res, next) {
  console.log('Time:', Date.now(), res.locals);
  //res.locals.grab = grab
  next();
});
// respond with "Hello World!" on the homepage
app.get('/', function (req, res) {
  res.send('Hello World!');
});

// accept POST request on the homepage
app.post('/', function (req, res) {
  res.send('Got a POST request');
});

// accept PUT request at /user
app.put('/user', function (req, res) {
  res.send('Got a PUT request at /user');
});

// accept DELETE request at /user
app.all('/user|camion|koko', function (req, res, next) {
  //res.send('Got a DELETE request at /user o sie');
  console.log("DITY");
  next();
}, demo.sencillo);
var cb0 = function (req, res, next) {
  console.log('CB0');
  next();
}

var cb1 = function (req, res, next) {
  console.log('CB1');
  next();
}

var cb2 = function (req, res) {
  res.send('Hello from C!');
}

app.get('/example/c', [cb0, cb1, cb2]);
var server = app.listen(3000, function () {

  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);

});