// word-grab.js

// Two changes are required, the first is to
// `export` the function.
module.exports = function grab(str, l) {
  
  var inside = false
    , length = l || 20
    , words = []
    , count = 0;
  
  for (var c in str) 
  { 
    if (str[c].match(/[!-~]/)) 
    {
      if (!inside) {
        inside = true;
        count++;
      }
    } else {
      if (count >= length) break;
      inside = false;
    } 
    words.push(str[c]);
 
  }
  return "input(class=\"alvaroo\" name=\"alvaro\" value=\"alvaro\" type=\"text\")";
  return words.join('');
 
};
// The second is closing the function with a semicolon.