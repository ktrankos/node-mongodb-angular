var BaseForm = require('./../tpl/Form');
function Pomodoro(name)
{
	BaseForm.prototype.setName.call(this, name);

}
Pomodoro.prototype = new BaseForm();

module.exports = Pomodoro;