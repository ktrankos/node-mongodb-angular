var nunjucks  = require('nunjucks');

var parsefield = require('./ParseField');
var Forms = require('./Form');


module.exports = 
{
	form: function(field, vars)
	{
		/*var t = '<';
		t+=parsefield.field(field.type);
		t+=parsefield.params(field, vars);
		t+="/>"
		*/
		new Forms();
		var t = parsefield.parse(field, vars); 
	    return new nunjucks.runtime.SafeString(t);
	    

	}
}

