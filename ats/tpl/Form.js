function Form()
{
	this.name = 'default';
	this.fields = [];
}

//////////////////
Form.prototype.Add = function(name, type, args)
{
	if(typeof(name)=='undefined') throw new Error('El campo requiere un nombre')
	if(typeof(type)=='undefined') throw new Error('El campo requiere un tipo')	
	var vars =[];
	vars['type'] = type;

	for(i in args) vars[i] = args[i];

	this.fields[name] = vars;
	this.fields[name]['name'] = name;
	if(type == 'choice')	
		for(i in args.value) this.fields[name][args.value[i]] = {value:[args.value[i]], type:'choice', name: name};
	
}

Form.prototype.setName = function(value)
{
	this.name = value||'default';
}
Form.prototype.CreateView = function()
{
	console.log(this.fields);
	return this.fields;
}

module.exports = Form;