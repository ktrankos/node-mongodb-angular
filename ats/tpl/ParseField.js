module.exports = 
{
	parse: function(field, vars)
	{
		var t = "";
		t+= this.field(field, vars);
		return t;
	},
	field: function(field, vars)
	{

		if(field['type']=="text") return '<input type="text" '+this.params(field, vars)+' />';
		if(field['type']=='choice')
		{
			var i = "";
			for(s in field['value'])
			{
				i+= '<input type="';
				if(field.multiple) i+='checkbox';
				else i+='radio'
				i+='" value="'+field['value'][s]+'" name="'+field['name']+'" />';	
			}
			return i;

		}

	},
	params: function (field, vars)
	{
		var attrs = [];
		for(i in field) if(i!='type') attrs[i] = field[i];
		for(i in vars) attrs[i] = vars[i];
		var t = '';	
		for(i in attrs) t+=' '+i+'="'+attrs[i]+'" ';
		return t;
	}
}